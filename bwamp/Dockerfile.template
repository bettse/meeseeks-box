# base-image for node on any machine using a template variable,
# see more about dockerfile templates here: https://www.balena.io/docs/learn/develop/dockerfile/#dockerfile-templates
# and about balena base images here: https://www.balena.io/docs/reference/base-images/base-images/
FROM balenalib/%%BALENA_MACHINE_NAME%%-node:12-buster-build as build

# Build backend (pull down packages)
COPY package.json .
COPY package-lock.json .

# This install npm dependencies on the balena build server,
# making sure to clean up the artifacts it creates in order to reduce the image size.
RUN JOBS=MAX npm install --production --unsafe-perm && npm cache verify && rm -rf /tmp/*

# The run time container that will go to devices
FROM balenalib/%%BALENA_MACHINE_NAME%%-node:12-buster-run

# use `install_packages` if you need to install dependencies,
# for instance if you need git, just uncomment the line below.
RUN install_packages mpg123 libraspberrypi-bin alsa-utils sox

WORKDIR /usr/src/app

COPY --from=build ./node_modules ./node_modules

COPY index.js index.js
COPY sprites.js sprites.js
COPY bwamps.js bwamps.js
COPY package.json package.json
COPY sounds.wav sounds.wav

# server.js will run when container starts up on the device
CMD ["npm", "start"]
