// slightly modified version of https://bwamp.me/s/js/bwamps.js
// export
var bwamps = {},
  dangerColors = ["dark-green", "orange", "dark-red"],
  bwampsLeft = [
    {
      id: "👏",
      danger: 0, volume: 1,
      sound: "applause-lite",
    },
    {
      id: "👏👏👏",
      danger: 0, volume: 0.8,
      sound: "applause-more",
    },
    {
      id: "😮",
      danger: 0, volume: 0.8,
      sound: "oooh",
    },
    {
      id: "💪",
      danger: 1, volume: 1,
      sound: "powerful",
    },
    {
      id: "🥁🥁🥁",
      danger: 1, volume: 1,
      sound: "drum-roll",
    },
    {
      id: "🎉",
      danger: 1, volume: 0.7,
      sound: "bwamp",
    },
    {
      id: "🎉🎉",
      danger: 1, volume: 0.7,
      sound: "bwamp-2",
    },
    {
      id: "🎉🎉🎉",
      danger: 1, volume: 0.7,
      sound: "bwamp-3",
    },
    {
      id: "😂🤭😂",
      danger: 2, volume: 1,
      sound: "lots-of-laughs",
    },
    {
      id: "⛏💀",
      danger: 2, volume: 1,
      sound: "minecraft-death",
    },
    {
      id: "🍖📯🔁",
      danger: 2, volume: 1,
      sound: "ham-horn-multi",
    },
  ],
  bwampsRight = [
    {
      id: "👍",
      danger: 0, volume: 1,
      sound: "yes",
    },
    {
      id: "👎",
      danger: 0, volume: 1,
      sound: "no",
    },
    {
      id: "😻",
      danger: 0, volume: 1,
      sound: "awww",
    },
    {
      id: "💰",
      danger: 1, volume: 1,
      sound: "cha-ching",
    },
    {
      id: "😰🎺",
      danger: 1, volume: 1,
      sound: "sad-trombone",
    },
    {
      id: "🥂🍻",
      danger: 1, volume: 1,
      sound: "cheers",
    },
    {
      id: "✨",
      danger: 1, volume: 0.5,
      sound: "twinkle",
    },
    {
      id: "🦢",
      danger: 1, volume: 1,
      sound: "goose",
    },
    {
      id: "🐴🐴🐴",
      danger: 2, volume: 1,
      sound: "horse",
    },
    {
      id: "🐄🐄🐄",
      danger: 2, volume: 1,
      sound: "cowpaths",
    },
    {
      id: "🐈👎👎",
      danger: 2, volume: 1,
      sound: "nonono-cat",
    },
  ];

module.exports = {bwampsLeft, bwampsRight};
