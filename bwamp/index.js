const { spawn } = require("child_process");
const WebSocket = require('ws');
const fetch = require('node-fetch');
const HTMLParser = require('node-html-parser');
const {sprite: sprites} = require('./sprites');
const {bwampsLeft, bwampsRight} = require('./bwamps');

const origin = 'https://bwamp.me';
const {
  csrf = 'I80hhRhXsdKJTYCid4rE',
  u = 'BB5zaY2xc1A5UK8QXXfK3ajT10fTgHj02Y6D4nSD550=,eyJJRCI6IlVHUlRUNFc3MiIsIkVtYWlsIjoiYmV0dHNlQGZhc3RtYWlsLmZtIiwiTmFtZSI6IkVyaWMgQmV0dHMiLCJQaWN0dXJlIjoiIiwiTGFzdEF1dGhlbnRpY2F0ZWQiOiIyMDIxLTEwLTE5VDIwOjE5OjE0Ljg2MDI1MzQwNFoiLCJQcm92aWRlciI6InNsYWNrIiwiT3RoZXJQcm9wZXJ0aWVzIjpudWxsfQo=',
  channel = 'fastmail.fm',
} = process.env

const Cookie = `CookieConsent=1; u=${u}; csrf=${csrf}`;

const bwamps = {};
bwampsRight.forEach(bwamp => bwamps[bwamp.id] = bwamp)
bwampsLeft.forEach(bwamp => bwamps[bwamp.id] = bwamp)

async function playSound(emoji) {
  const bwamp = bwamps[emoji];
  if (!bwamp) {
    throw new Error(`no bwamp for ${emoji}`);
  }
  const { sound, volume } = bwamp;
  const sprite = sprites[sound];
  if (!sprite) {
    throw new Error(`no sprite for ${sound}`);
  }

  // Durations are in ms
  const [start, duration] = sprite.map(x => x/1000);
  const parameters = ['sounds.wav', 'vol', volume, 'trim', start, duration];
  return new Promise((resolve, reject) => {
    const process = spawn('play', parameters, {stdio: 'ignore'});
    process.on("close", () => {
      resolve();
    })
    process.on("error", (error) => {
      reject(error);
    });
  });
}

async function getVersion() {
  try {
    const response = await fetch(origin);
    if (!response.ok) {
      throw new Error(`error getting ${origin}`);
    }
    const body = await response.text();
    const root = HTMLParser.parse(body);
    const version_meta = root.querySelector('meta[name=version]');
    const version = version_meta.getAttribute('content')
    return version
  } catch (e) {
    console.log(e);
  }
  return ''
}

let ws;

function setupWS(version) {
  const url = `wss://bwamp.me/stream/${channel}/?v=${version}&csrf=${csrf}`;
  if (ws) {
    ws.removeAllListeners();
  }
  ws = new WebSocket(url, {
    origin,
    headers: { Cookie }
  });

  ws.on('open', () => {
    console.log('ws open');
  });

  ws.on('message', (event) => {
    console.log('received: %s', event);
    try {
      const { who, what, sound } = JSON.parse(event)
      switch (what) {
        case 0: // Join
          break;
        case 1: // Leave
          break;
        case 2: // Sound
          playSound(sound);
        default:
          break;
      }
    } catch (e) {
      console.log(e);
    }
  });

  ws.on('close', () => {
    console.log('disconnect');
    setTimeout(() => {
      setupWS(version);
    }, 1000);
  });

  ws.on('error', (e) => {
    console.log('error', e);
    setTimeout(() => {
      setupWS(version);
    }, 1000);
  });

  setInterval(() => {
    if (ws) {
      ws.send(JSON.stringify({
        what: 3, // SUPPRESSED
      }));
    }
  }, 5 * 60 * 1000);
}

function parseCookie() {
  try {
    const [raw_sig, raw_jwt] = u.split(',')
    const sig = Buffer.from(raw_sig, 'base64');
    const jwt = Buffer.from(raw_jwt, 'base64').toString('ascii');
    const token = JSON.parse(jwt);
    const { ID } = token // Math.random().toString(36).substr(2,9);
    console.log(ID)

    console.log(Buffer.from(sig), jwt)
  } catch (e) {
    console.log(e);
  }
}

async function main() {
  const version = await getVersion();
  setupWS(version);

  return '';
}

main().then(console.log).catch(console.error);
